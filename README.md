Master : les cours


# EXO_PRODUCT_BACKLOG_0924 : Votre backlog produit

Identifier les Users Stories de votre future produit

# EXO_RETRO_1_0930

# EXO_BACKLOG_0930

# EXO_ESTIMATIONS

# EXO_SPRINT_PLANNING

1. Matrice des compétences (Analyse, Architecture, Developemment et Test) des 5 membres de l'équipe

| Compétences        | Jhon   | Alice  | Gerard | Sylvia |  Bob   |
| ------------------ |:------:|:------:|:------:|:------:|:------:|
| Analyse            | \*\*\* | \*  |  |  | \*\*  | 
| Architecture       | \* |   | \*\*\* | \*\*\* | \*  | 
| Developemment      |  \*\*\* | \*  |  | \*\* | \*  | 
| Test               |  \*\*\* | \*  | \*\* | \*\*\* | \*  | 


2. Pour chaque User Storie, liste des tâches avec :
+ Descrition
+ compétences
+ Taille (Small, Medium, Large)