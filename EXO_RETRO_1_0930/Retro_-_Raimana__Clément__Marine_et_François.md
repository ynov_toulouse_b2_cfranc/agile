 # Retrospective

| Stop  | Continue  | Start |
|:-----:|:-----:|:-----:|
| Bruit de fond (bavardage) | Participation lors de la formation |  |
|    |  Les exercices pratiques    |   |
| Interdiction machine à café et Port du masque moins strict|  Les pauses et les cours en présentiels | Manger dans l'établissement le midi (hiver) : respect des règles sanitaires. |



Les 3 actions choisis : 

- Arrêter le bruit de fond permanent dû aux bavardages.

- Continuer et privilégier la pratique à la théorie.

- Continuer d'avoir des cours en présentiels et non en visio-conférence.