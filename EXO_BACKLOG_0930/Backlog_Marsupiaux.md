## Projet : 
Site e-commerce pour une marque de vêtement :
- Mise en valeur des valeurs des produits sur internet / Promo / délai de livraison
- Expérience Utilisateurs agréable / site moderne et responsive
- Différentes collections
- Stock de vêtement restant / Prix / tailles / couleurs /
- Gestion facile de la BDD

## Backlog : 

1.	EN TANT QUE visiteur JE VEUX visualiser un produit, sa taille et ses couleurs possibles AFIN DE découvrir les nouveautés de la marque.
2.	EN TANT QUE visiteur ou client JE VEUX ajouter un article à mon panier AFIN DE passer une commande ultérieurement.
3.	EN TANT QUE visiteur JE VEUX créer un compte AFIN D’ avoir un espace privée et des produits ciblés.
4.	EN TANT QUE client JE VEUX effectuer une commande AFIN DE renouveler ma garde-robe.
5.	EN TANT QUE client fidèle JE VEUX être mis au courant des promotions AFIN DE bénéficier de prix avantageux.
6.	EN TANT QUE client JE VEUX un moyen de contact avec la marque AFIN DE poser mes questions sur ma (futur) commande.
7.	EN TANT QUE client JE VEUX suivre ma commande AFIN DE connaître son avancement et sa date de livraison.


8.	EN TANT QU'admin JE VEUX un système de paiement sécurisé AFIN DE rassurer mes clients.
9.	EN TANT QU’admin JE VEUX une base de données organisé AFIN DE facilité la gestion des stocks.
10.	EN TANT QU’admin JE VEUX un site ergonomique, moderne et responsive AFIN D’avoir une expérience utilisateurs de qualité.
11.	EN TANT QU’admin JE VEUX optimiser mon référencement (SEO) AFIN D’avoir une meilleure visibilité.
